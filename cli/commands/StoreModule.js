const capitalize = require('lodash.capitalize');
const utils = require('../utils');

const create = ({ name, url }) => {
  const destFolder = `src/features/${name}`;

  const STORE_FOLDER = `${destFolder}/store`

  const templateOptions = { moduleName: name, moduleNameCap: capitalize(name), moduleUrl: url }

  const files = [
    { destFile: `${STORE_FOLDER}/functions.js`, template: 'storeFunctions.ejs' },
    { destFile: `${STORE_FOLDER}/index.js`, template: 'storeIndex.ejs' },
  ]

  utils.mkdir(destFolder);
  utils.mkdir(STORE_FOLDER);

  files.forEach(({ destFile, template }) => {
    utils.createFile(destFile, template, templateOptions)
  })
};

module.exports = {
  create,
};
