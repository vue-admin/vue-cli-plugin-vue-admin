const capitalize = require('lodash.capitalize');
const utils = require('../utils');
const storeModule = require('./StoreModule');

const create = ({ name, url }) => {
  const destFolder = `src/features/${name}`;

  const VIEWS_FOLDER = `${destFolder}/views`

  const templateOptions = { moduleName: name, moduleNameCap: capitalize(name) }

  const files = [
    { destFile: `${VIEWS_FOLDER}/${name}.vue`, template: 'pageEntry.ejs' },
    { destFile: `${VIEWS_FOLDER}/${name}List.vue`, template: 'pageList.ejs' },
    { destFile: `${VIEWS_FOLDER}/${name}Edit.vue`, template: 'pageEdit.ejs' },
    { destFile: `${VIEWS_FOLDER}/${name}View.vue`, template: 'pageView.ejs' },
    { destFile: `${destFolder}/routes.js`, template: 'router.ejs' },
    { destFile: `${destFolder}/form.js`, template: 'form.ejs' },
  ]

  utils.mkdir(destFolder);
  utils.mkdir(VIEWS_FOLDER);

  files.forEach(({ destFile, template }) => {
    utils.createFile(destFile, template, templateOptions)
  })

  storeModule.create({ name, url })
};

module.exports = {
  create,
};
