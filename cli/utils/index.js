const fs = require('fs');
const path = require('path');
const ejs = require('ejs');

const mkdir = (dest) => {
  if (!fs.existsSync(dest)) {
    try {
      fs.mkdirSync(path.resolve(dest), { recursive: true });
    } catch (err) {
      console.log(err);
      if (err.code !== 'EEXIST') throw err;
    }
  }
};

const createFile = (destFile, template, ejsOptions) => {
  const entryPageContent = ejs.render(
    fs.readFileSync(`node_modules/@josercl/vue-cli-plugin-vue-admin/cli/templates/${template}`, 'utf-8'),
    ejsOptions
  )
  fs.writeFileSync(destFile, entryPageContent)
}

module.exports = {
  mkdir,
  createFile
}
