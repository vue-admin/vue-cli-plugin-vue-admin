export default {
  'admin-navbar': {
    classes: {
      main: 'navbar navbar-light px-3 border-bottom',
      brand: 'font-weight-bold',
      toggle: 'navbar-toggler d-inline-block d-md-none',
      toggleMenu: 'd-flex d-md-none flex-basis-100 flex-column g-3 text-center mt-3',
      menuWrapper: 'd-none d-lg-block',
      menuLeft: '',
      menuRight: 'ml-auto',
    },
  },
  'admin-layout': {
    classes: {
      layout: 'd-flex flex-column min-vh-100 vw-100',
      main: 'd-flex flex-row flex-grow-1',
      menu: 'p-4 bg-light border-right d-none d-lg-flex',
    },
  },
  'admin-footer': {
    classes: {
      main: 'p-4 text-center text-muted border-top bg-light',
    },
  },
  'admin-menu': {
    classes: {
      wrapper: 'nav nav-pills flex-column',
      itemWrapper: 'nav-item mb-2',
      item: 'nav-link',
    },
  },
  'admin-table': {
    classes: {
      wrapper: 'table-responsive',
      main: 'table table-hover',
      head: 'bg-light',
      th: 'p-3 text-left',
      td: 'p-2 text-nowrap',
      tr: '',
      actionsWrapper: 'w-100 d-inline-flex flex-column flex-md-row',
    },
  },
  'admin-pagination': {
    classes: {
      wrapper: 'w-100 d-flex flex-row mb-4 justify-content-end',

      selectorWrapper: 'd-md-none d-inline-block',
      selector: 'form-select',
      pagesWrapper: 'pagination mb-0 d-none d-md-inline-flex',

      itemWrapper: 'page-item',
      item: 'page-link',
      activeItem: 'active'
    },
  },
};
