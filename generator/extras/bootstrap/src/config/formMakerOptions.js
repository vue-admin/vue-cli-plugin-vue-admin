export default {
  classes: {
    'form-row': 'row g-3 mb-3',
    'form-column': 'col-12 col-md',
    'form-label': 'fw-bold form-label',

    'input-group': 'input-group',
    'input-wrapper': '',
    'input-error': 'is-invalid',
    'input': 'form-control',

    'error': 'invalid-feedback',
    'help-text': 'mt-1 text-muted fs-6',
    'submit-button': 'btn btn-primary',
  }
}
