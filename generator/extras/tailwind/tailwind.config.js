module.exports = {
    theme: {},
    plugins: [],
    content: [
        './src/**/*.{vue,html,js}',
        './node_modules/@josercl/form-maker/src/lib/**/*.{js,vue}',
        './node_modules/@josercl/admin-components/src/lib/**/*.{js,vue}',
    ],
};
