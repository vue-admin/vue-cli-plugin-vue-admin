export default {
  'admin-navbar': {
    classes: {
      main: 'flex flex-row flex-wrap items-center justify-between bg-gray-100 border-b border-gray-300 p-4',
      brand: 'font-bold text-lg',
      toggle: 'inline-block md:hidden border rounded-md border-gray-300 border p-2',
      toggleMenu: 'basis-full flex flex-col space-y-2 text-center',
      menuWrapper: 'hidden md:flex md:flex-row md:flex-grow',
      menuLeft: 'mr-auto',
      menuRight: 'ml-auto',
    },
  },
  'admin-layout': {
    classes: {
      layout: 'flex flex-col min-h-screen',
      main: 'flex flex-row flex-grow',
      menu: 'p-4 hidden lg:block w-1/5 border-r border-gray-300',
    },
  },
  'admin-footer': {
    classes: {
      main: 'p-4 text-center text-gray-800 border-t border-gray-300',
    },
  },
  'admin-menu': {
    classes: {
      wrapper: 'flex flex-col flex-grow',
      itemWrapper: '',
      item: 'p-4 rounded-md whitespace-nowrap inline-block w-full hover:bg-blue-600 hover:text-white',
    },
  },
  'admin-table': {
    classes: {
      wrapper: 'w-full overflow-x-scroll md:overflow-auto',
      main: 'table border-collapse w-full',
      head: 'bg-gray-100 border-b-2 border-gray-300',
      th: 'p-3 text-left',
      td: 'p-3 border-b border-gray-200',
      tr: 'hover:bg-gray-100 cursor-pointer',
      actionsWrapper: 'w-full inline-flex flex-col md:flex-row space-y-2 md:space-y-0',
    },
  },
  'admin-pagination': {
    classes: {
      wrapper:
        'flex flex-row justify-end flex-space-y-4 mb-4',
      selectorWrapper: 'md:hidden',
      selector:
        'bg-white px-4 py-2 border border-gray-200 rounded-md ring-0 outline-none',
      pagesWrapper: 'hidden md:inline-flex flex-row',
      itemWrapper: 'border border-gray-200',
      item: 'inline-block py-2 px-3 text-center',
      activeItem: '!bg-blue-500 !text-white'
    },
  },
};
