export default {
  classes: {
    'form-row': 'flex flex-row mb-4 space-x-4',
    'form-column': 'flex-grow',
    'form-label': 'block font-bold mb-2',

    'input-group': '',
    'input-wrapper': '',
    'input-error': '!border-red-500 !focus:border-red-500',
    'input': 'px-4 py-2 border-2 border-gray-300 w-full rounded-md focus:ring-0 focus:outline-none focus:border-blue-500 disabled:bg-gray-100 disabled:text-gray-400',

    'error': 'mt-1 text-red-600',
    'help-text': 'mt-1 text-gray-500/75',
    'submit-button': 'rounded-md bg-blue-500 text-white hover:bg-blue-700 px-4 py-2',
  }
}
