import { createRouter, createWebHistory } from "vue-router";
import Cookies from "js-cookie";
import constants from "../config/constants";
import Login from "../views/Login.vue";

const requireRoutes = import.meta.globEager("../features/**/routes.js"); //require.context('../features', true, /routes\.js$/);
const childRoutes = [];

Object.keys(requireRoutes).forEach(async (path) => {
  const mod = requireRoutes[path];
  childRoutes.push(mod.default);
});

const routes = [
  {
    path: "/",
    name: "Home",
    redirect: { name: "Dashboard" },
  },
  {
    path: "/auth/login",
    name: "Login",
    component: Login,
  },
  {
    path: "/admin",
    component: () => import(/* webpackChunkName: "admin-layout" */ "../views/Admin.vue"),
    meta: { requiresAuth: true },
    children: [
      {
        path: "dashboard",
        name: "Dashboard",
        component: () => import(/* webpackChunkName: "dashboard" */ "../views/AdminDashboard.vue"),
      },
      ...childRoutes,
      {
        path: "permisos",
        name: "NoPermiso",
        component: () => import(/* webpackChunkName: "not-authorized" */ "../views/NotAuthorized.vue"),
      },
      {
        path: "/:pathMatch(.*)*",
        component: () => import(/* webpackChunkName: "not-found" */ "../views/NotFoundPage.vue"),
      },
    ],
  },
  {
    path: "/forbidden",
    component: () => import(/* webpackChunkName: "not-authorized" */ "../views/NotAuthorized.vue"),
  },
  {
    path: "/:pathMatch(.*)*",
    component: () => import(/* webpackChunkName: "not-found" */ "../views/NotFoundPage.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(),
  linkActiveClass: "active",
  routes,
});

router.beforeEach((to, from, next) => {
  const token = Cookies.get(constants.SESSION_COOKIE);

  if (to.name === "Login" && Boolean(token)) {
    next(from || { name: "Home" });
    return;
  }

  if (to.path.startsWith("/admin") || to.meta.requiresAuth) {
    if (!token) {
      next({ name: "Login" });
      return;
    }
  }

  next();
});

export default router;
