import { defineStore } from "pinia";
import * as functions from "./functions";

const useUserStore = defineStore({
  id: "user",
  state: () => ({
    user: {
      name: "",
      email: "",
    },
    loading: false,
    loggedIn: false,
  }),
  actions: {
    async loginUser(data) {
      this.loading = true;
      try {
        await functions.doLogin(data);

        this.getUser();

        this.loggedIn = true;
      } catch (e) {
        console.error(e);
      }
      this.loading = false;
    },
    async getUser() {
      const { data } = await functions.getUserProfile();
      this.user = data;
    },
    async logout() {
      await functions.logout();
    },
  },
});

export default useUserStore;
