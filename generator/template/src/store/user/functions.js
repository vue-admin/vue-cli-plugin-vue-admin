import Cookies from "js-cookie";
import constants from "../../config/constants";
import httpClient from "../../config/httpClient";

export const doLogin = async ({ username, password }) => {
  const loginData = {
    username,
    password,
    grant_type: "password",
    client_id: constants.CLIENT_ID,
    client_secret: constants.CLIENT_SECRET,
  };

  const response = await httpClient.post("../auth/login", loginData);
  Cookies.set(constants.SESSION_COOKIE, response.access_token);
};

export const getUserProfile = async () => await httpClient.get("usuarios/me");

export const logout = async () => {
  await httpClient.post("../auth/logout");
  Cookies.remove(constants.SESSION_COOKIE);
};
