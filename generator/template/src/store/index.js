import constants from "../../src/config/constants";
import { defineStore } from "pinia";
import cloneDeep from "lodash.clonedeep";

export const processValidationErrors = (errorMessages) => {
  const errors = {};

  Object.keys(errorMessages).forEach((field) => {
    const fieldError = errorMessages[field];
    if (Array.isArray(fieldError)) {
      errors[field] = fieldError[0];
    } else {
      errors[field] = fieldError;
    }
  });

  return errors;
};

export const baseState = {
  list: {},
  loading: false,
  currentPage: 1,
  totalPages: 1,
  pageSize: constants.DEFAULT_PAGE_SIZE,
  errors: {},
  updating: false,
  currentItem: {},
}

export const generateStore = (
  storeId,
  { deleteItem, getItem, getList, saveItem },
  extraState = {},
  extraActions = {},
) => defineStore({
  id: storeId,
  state: () => ({ ...cloneDeep(baseState), ...extraState }),
  actions: {
    async getList(params, paginated = true) {
      this.loading = true;

      const response = await getList(params);

      const data = response[constants.PAGINATION_DATA]

      if (paginated) {
        const meta = response[constants.PAGINATION_META]

        const currentPage = Number.parseInt(
          response[constants.PAGINATION_META][constants.PAGINATION_CURRENT_PAGE],
          10
        );

        const totalPages = Number.parseInt(meta[constants.PAGINATION_TOTAL_PAGES], 10)
        const pageSize = Number.parseInt(meta[constants.PAGINATION_PER_PAGE], 10)

        this.$patch({
          currentPage,
          totalPages: Number.parseInt(meta[constants.PAGINATION_TOTAL_PAGES], 10),
          pageSize: Number.parseInt(meta[constants.PAGINATION_PER_PAGE], 10),
          loading: false,
          list: {
            [currentPage]: data,
          },
        });
        return
      }

      this.$patch({
        loading: false,
        list: {
          [this.currentPage]: data,
        },
      });
    },
    async getItem(id) {
      this.loading = true;

      const { data } = await getItem(id);

      this.$patch({
        currentItem: data,
        loading: false,
      });
    },
    async saveItem(item) {
      this.loading = true;

      try {
        const { data } = await saveItem(item);

        const newList = cloneDeep(this.list);

        if (item.id) {
          Object.keys(newList).map((key) => {
            newList[key] = newList[key].map((item) => {
              if (item.id == data.id) {
                return data;
              }
              return item;
            });
          });
        } else {
          newList[this.currentPage].push(data);
        }
        this.$patch({
          loading: false,
          list: newList,
        });
      } catch (e) {
        this.loading = false;
        await this.setErrors(e.data.errors)
        throw e;
      }
    },
    async resetItem() {
      this.currentItem = {};
      return Promise.resolve();
    },
    async deleteItem(itemToDelete) {
      await deleteItem(itemToDelete)
      const newList = cloneDeep(this.list);

      Object.keys(newList).map((key) => {
        newList[key] = newList[key].filter((item) => item.id != itemToDelete.id)
      });

      this.list = newList
    },
    async changePageSize(size) {
      this.$reset()

      this.pageSize = Number.parseInt(size, 10)
    },
    async resetList() {
      this.list = {};
    },
    async setErrors(errors) {
      this.errors = processValidationErrors(errors);
    },
    async resetErrors() {
      this.errors = {};
      return Promise.resolve();
    },
    ...extraActions,
  },
});
