export const updateFormErrors = (errors, formFields) => {
  if (Object.keys(errors).length === 0) {
    formFields.forEach((row) => {
      row.forEach((field) => {
        delete field.error;
      });
    });
  } else {
    Object.keys(errors).forEach((key) => {
      formFields.forEach((row) => {
        row.forEach((field) => {
          if (field.name === key) {
            field.error = errors[key];
          }
        });
      });
    });
  }
};
