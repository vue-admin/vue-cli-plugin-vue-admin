/**
 * Object that contains global constants for project
 * @type {{PAGINATION_DATA: string, DEFAULT_LANG: (*|string), CLIENT_SECRET: *, PAGINATION_TOTAL_PAGES: string, API_URL: *, CLIENT_ID: *, PAGINATION_PER_PAGE: string, APP_NAME: (*|string), SESSION_COOKIE: (*|string), SHORT_APP_NAME: (*|string), PAGINATION_META: string, PAGINATION_CURRENT_PAGE: string}}
 */
const constants = {
  APP_NAME: import.meta.env.VITE_APP_NAME || "Admin",
  SHORT_APP_NAME: import.meta.env.VITE_SHORT_APP_NAME || "A",
  SESSION_COOKIE: import.meta.env.VITE_SESSION_COOKIE || "vue-admin.session",
  API_URL: import.meta.env.VITE_API_URL,
  CLIENT_ID: import.meta.env.VITE_CLIENT_ID,
  CLIENT_SECRET: import.meta.env.VITE_CLIENT_SECRET,
  DEFAULT_PAGE_SIZE: Number.parseInt(import.meta.env.VITE_PAGE_SIZE, 10) || 25,
  PAGINATION_META: "meta",
  PAGINATION_DATA: "data",
  PAGINATION_CURRENT_PAGE: "current_page",
  PAGINATION_TOTAL_PAGES: "last_page",
  PAGINATION_PER_PAGE: "per_page",
  DEFAULT_LANG: import.meta.env.VITE_DEFAULT_LANG || "en",
};

export default constants;
