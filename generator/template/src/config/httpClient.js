import axios from "axios";
import Cookies from "js-cookie";
import router from "@/router";
import constants from "./constants";

const httpClient = axios.create({
  baseURL: constants.API_URL,
});

httpClient.interceptors.request.use((config) => {
  const accessToken = Cookies.get(constants.SESSION_COOKIE);
  if (accessToken) {
    config.headers["Authorization"] = `Bearer ${accessToken}`;
  }
  return config;
});

httpClient.interceptors.response.use(
  (response) => response.data,
  (error) => {
    if (error.response.status === 401) {
      Cookies.remove(constants.SESSION_COOKIE);
      //const router = useRouter()
      return router.push({ name: "Login" });
    }
    return Promise.reject(error.response);
  }
);

export default httpClient;
