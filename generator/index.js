/**
 * @file Initial configuration for project structure and dependencies
 */
const { EOL } = require("os");
const fs = require("fs");

function stylesDependencies(cssFramework) {
  if (cssFramework === 'bootstrap') {
    return {
      'bootstrap': '^5.1.3',
      "sass": "^1.49.9"
    };
  }
  if (cssFramework === 'tailwind') {
    return {
      'tailwindcss': '^3.0.23',
      'postcss': '^8.4.8',
      'autoprefixer': '^10.4.2'
    };
  }
}

function updatePackage(api, options) {
  /**vue-admin necessary dependencies*/
  let dependencies = {
    "@josercl/form-maker": "1.1.0-beta04",
    "@josercl/admin-components": "1.0.0-beta04",
    ...stylesDependencies(options.cssFramework),
    "axios": "^0.26.0",
    "js-cookie": "^3.0.1",
    "lodash.clonedeep": "^4.5.0",
    "pinia": "^2.0.11",
    "vue": "^3.2.31",
    "vue-router": "^4.0.12"
  };

  /**Development dependencies*/
  api.extendPackage({
    dependencies,
    scripts: {
      "admin-create": "vue-admin-create"
    }
  });
}


module.exports = (api, options) => {
  updatePackage(api, options);

  api.render("./template");

  api.injectImports(api.entryFile, `import router from "./router";`);
  api.injectImports(api.entryFile, `import { createPinia } from "pinia";`);
  api.injectImports(api.entryFile, `import AdminComponents from "@josercl/admin-components";`);
  api.injectImports(api.entryFile, `import FormMaker from "@josercl/form-maker";`);

  api.injectImports(api.entryFile, `import adminComponentsOptions from "./config/adminComponentsOptions";`);
  api.injectImports(api.entryFile, `import formMakerOptions from "./config/formMakerOptions";`);

  if (options.cssFramework === 'tailwind') {
    api.injectImports(api.entryFile, `import "./assets/main.css";`);
  } else {
    api.injectImports(api.entryFile, `import "./assets/main.scss";`);
  }

  api.onCreateComplete(() => {
    let content = fs.readFileSync(api.entryFile, { encoding: 'utf-8'});
    content = content.replace('createApp(App)', 'createApp(App)\n.use(createPinia())')
    content = content.replace('createApp(App)', 'createApp(App)\n.use(router)')
    content = content.replace('createApp(App)', 'createApp(App)\n.use(FormMaker, formMakerOptions)')
    content = content.replace('createApp(App)', 'createApp(App)\n.use(AdminComponents, adminComponentsOptions)')
    fs.open(api.entryFile, 'w', (err, file) => {
      if (err) throw err;
      fs.writeFileSync(file, content);
    });
  });

  api.render(`./extras/${options.cssFramework}`);

  //api.injectRootOptions(api.entryFile, ["store", "router"]);

  api.exitLog('Done!!', 'done');

  api.genJSConfig(options);
};
