/**
 * @file Array of questions initial configuration trough prompts.js
 */
module.exports = [
  {
    name: 'cssFramework',
    type: 'list',
    message: 'CSS Framework',
    choices: [
      { name: 'Bootstrap', value: 'bootstrap' },
      { name: 'Tailwind CSS', value: 'tailwind'},
    ],
    default: 'bootstrap',
  },
];
